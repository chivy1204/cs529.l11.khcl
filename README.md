# CS529.L11.KHCL

# 09/09/2020

# Nội dung buổi học: Phân nhóm, lên ý tưởng cho một ứng dụng

# Các hoạt động của các thành viên trong nhóm:

# - Nguyễn Chí Vỹ: Giới thiệu nhóm + đặt câu hỏi

# - Nguyễn Hữu Quyền: Nêu ý tưởng ứng dụng

# 16/09/2020

# Nội dung buổi học: nói về các từ khóa conference, workshop, grand challenge và tutorial

# Trả lời câu hỏi của thầy về tóm tắt nội dung các từ khóa trên

# Các hoạt động của các thành viên trong nhóm:

# - Nguyễn Đức Toàn: Hướng nghiên cứu, bài toán quan tâm, cho biết input output

# 23/09/2020

# Nội dung buổi học: nói về các bài báo khoa học và cách để tìm tài liệu nghiên cứu khoa học

# 03/10/2020

# Nội dung buổi học: tìm hiểu thêm về cái bài nghiên cứu khoa học, định hướng hướng nghiên cứu ứng dụng cho công việc sau này

# 07/10/2020

# Nội dung buổi học: hướng dẫn cách làm survey

# 14/10/2020

# Nội dung buổi học: tìm hiểu Linear Regression

Bài tập về survey các hướng tiếp cận

# 21/10/2020

Các nhóm lên thuyết trình về survey mà mình đang nghiên cứu

# 04/11/2020

Giới thiệu các bài toán Natural Language Processing

# 18/11/2020

Sử dụng Logistic Regression để dự đoán từ tiếp theo trong câu chưa đủ ý

# 25/11/2020

Các thuật toán máy học cơ bản và bất đẳng thức Hoeffding

# 02/12/2020

Máy học là gì, mạng neural và Multilayer Perceptron

# 09/12/2020

Dùng các kỹ thuật chuyển đổi không gian để phân chia tuyến tính và kỹ thuật biến đổi phi tuyến + Hàm ReLU trong wide learning
